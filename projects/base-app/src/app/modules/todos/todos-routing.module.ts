import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddTodoComponent } from './components/add-todo/add-todo.component';
import { ShowTodoComponent } from './components/show-todo/show-todo.component';
import { TodosComponent } from './components/todos/todos.component';

const routes: Routes = [
  { path: '', component: TodosComponent },
  { path: 'show/:id', component: ShowTodoComponent },
  { path: 'add', component: AddTodoComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TodosRoutingModule { }
