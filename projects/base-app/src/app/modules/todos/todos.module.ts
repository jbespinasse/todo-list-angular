import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { todosReducer, todosFeatureKey } from './store/reducers/todo.reducer';
import { TodosEffects } from './store/effects/todos.effects';
import { TodosRoutingModule } from './todos-routing.module';
import { TodosComponent } from './components/todos/todos.component';
import {MatCardModule} from '@angular/material/card';
import {MatTableModule} from '@angular/material/table';
import {MatSelectModule} from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import { ShowTodoComponent } from './components/show-todo/show-todo.component';
import { routerReducer } from '@ngrx/router-store';
import { AddTodoComponent } from './components/add-todo/add-todo.component';
import { MatChipsModule } from '@angular/material/chips';
import { ComponentStore } from '@ngrx/component-store';

@NgModule({
  declarations: [
    TodosComponent,
    ShowTodoComponent,
    AddTodoComponent
  ],
  imports: [
    CommonModule,
    MatCardModule,
    MatTableModule,
    MatFormFieldModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    TodosRoutingModule,
    StoreModule.forFeature(todosFeatureKey, todosReducer),
    StoreModule.forFeature('router', routerReducer),
    EffectsModule.forFeature([TodosEffects]),
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatChipsModule
  ],
  bootstrap: [TodosComponent],
  exports: [],
  providers: [ComponentStore]
})
export class TodosModule {
}
