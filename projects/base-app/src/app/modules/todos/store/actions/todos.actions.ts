import { Update } from '@ngrx/entity';
import { createAction, props } from '@ngrx/store';
import { TodoModel } from '../../models/todo-model';

export enum TodosActionsType {
  load = '[Todos] Load Todos',
  loadTodosSuccess = '[Todos] Load Todos Success',
  loadTodosFailure = '[Todos] Load Todos Failure',
  update = '[Todos] Update Todo',
  updateTodoSuccess = '[Todos] Update Todo Success',
  updateTodoFailure = '[Todos] Update Todo Failure',
  loadId = '[Todos] Load Id',
  add = '[Todos] Add todo',
  addTodoSuccess = '[Todos] Add Todo Success',
  addTodoFailure = '[Todos] Add Todo Failure',
  todoLastAddFirst = '[Todos] Last add first',
}

export const loadTodos = createAction(
  TodosActionsType.load
);

export const loadTodosSuccess = createAction(
  TodosActionsType.loadTodosSuccess,
  props<{ todos: TodoModel[] }>()
);

export const loadTodosFailure = createAction(
  TodosActionsType.loadTodosFailure,
  props<{ error: any }>()
);

export const updateTodo = createAction(
  TodosActionsType.update,
  props<{ todo: Update<TodoModel> }>()
);

export const updateTodoSuccess = createAction(
  TodosActionsType.updateTodoSuccess,
  props<{ update: Update<TodoModel> }>()
);

export const updateTodoFailure = createAction(
  TodosActionsType.updateTodoFailure,
  props<{ error: any }>()
);

export const loadId = createAction(
  TodosActionsType.loadId,
  props<{ id: string }>()
);

export const addTodo = createAction(
  TodosActionsType.add,
  props<{ todo: TodoModel }>()
);

export const addTodoSuccess = createAction(
  TodosActionsType.addTodoSuccess,
  props<{ todo: TodoModel }>()
);

export const addTodoFailure = createAction(
  TodosActionsType.addTodoFailure,
  props<{ error: any }>()
);
