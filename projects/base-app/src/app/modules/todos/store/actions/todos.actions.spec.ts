import * as fromTodos from './todos.actions';

describe('loadTodoss', () => {
  it('should return an action', () => {
    expect(fromTodos.loadTodos().type).toBe('[Todos] Load Todoss');
  });
});
