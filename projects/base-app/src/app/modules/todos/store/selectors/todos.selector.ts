import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromTodos from '../reducers/todo.reducer';

export const selectTodosState = createFeatureSelector<fromTodos.TodosState>(fromTodos.todosFeatureKey);

export const selectTodoEntities = createSelector(
    selectTodosState,
    fromTodos.selectEntities
);

export const selectAllTodos = createSelector(
    selectTodosState,
    fromTodos.selectAll
);

export const allTodosSuccess = createSelector(
    selectTodosState,
    todosState => todosState.allTodosSuccess
);

export const getSelectedTodoId = createSelector(
    selectTodosState,
    fromTodos.getSelectedTodoId
);

export const getSelectedTodo = createSelector(
    selectTodoEntities,
    getSelectedTodoId,
    (todosDictionary: any, id) => todosDictionary[id]
);

export const selectId = createSelector(
    selectTodosState,
    state => state.selectedId
);

export const getAllTodosSortedByIdDesc = createSelector(
    selectAllTodos,
    (todosDictionary: any) => {
        todosDictionary.sort(fromTodos.sortByIdDesc);
        return todosDictionary.sort(fromTodos.sortByState);
    }
);
