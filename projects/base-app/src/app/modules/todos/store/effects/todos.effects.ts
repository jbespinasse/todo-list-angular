import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, exhaustMap, map, mergeMap, startWith, switchMap } from 'rxjs/operators';
import * as todoActions from '../actions/todos.actions';
import {  of } from 'rxjs';
import { TodoModel } from '../../models/todo-model';
import { TodosRepository } from '../../repositories/todos-repository';
import { Store } from '@ngrx/store';
import { TodosState } from '../reducers/todo.reducer';
import { ROUTER_NAVIGATED } from '@ngrx/router-store';
import { selectRouteParam } from '../selectors/router.selector';

@Injectable()
export class TodosEffects {

  constructor(private actions$: Actions,
    private todosRepository: TodosRepository,
    private store: Store<TodosState>) {}

  loadTodos$ = createEffect(() => this.actions$.pipe(
    ofType(todoActions.loadTodos),
    startWith(todoActions.loadTodos),
    mergeMap(() => this.todosRepository.getAll().pipe(
      map((todos: TodoModel[]) => todoActions.loadTodosSuccess({ todos })),
      catchError(() => of(todoActions.loadTodosFailure({ error: 'An error occur.' })))
    ))
  ));

  updateTodo$ = createEffect(() => this.actions$.pipe(
    ofType(todoActions.updateTodo),
    exhaustMap(({ todo }) => this.todosRepository.updateById(<number>todo.id, todo.changes).pipe(
      map((todoUpdated) => todoActions.updateTodoSuccess({ update: todoUpdated })),
      catchError(() => of(todoActions.updateTodoFailure({ error: 'An error occur.' })))
    ))
  ));

  loadIds$ = createEffect(() => this.actions$.pipe(
    ofType(ROUTER_NAVIGATED),
    switchMap(() => this.store.select(selectRouteParam('id') as any)),
    map((id: string) => todoActions.loadId({ id }))
  ));

  addTodo$ = createEffect(() => this.actions$.pipe(
    ofType(todoActions.addTodo),
    exhaustMap(({ todo }) => this.todosRepository.add(todo).pipe(
      map((todoAdd) => todoActions.addTodoSuccess({ todo: todoAdd })),
      catchError(() => of(todoActions.addTodoFailure({ error: 'An error occur.' })))
    ))
  ));
}
