import { Action, createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { TodoModel } from '../../models/todo-model';
import * as todoActions from '../actions/todos.actions';

export const todosFeatureKey = 'todos';

export function sortByState(a: TodoModel, b: TodoModel): number {
  return (b.state === 'done') ? -1 : 1;
}

export function sortByIdDesc(a: TodoModel, b: TodoModel): number {
  return a.id < b.id ? 1 : -1;
}

export interface TodosState extends EntityState<TodoModel> {
  allTodosSuccess: boolean;
  selectedId: string;
}

export const adapter: EntityAdapter<TodoModel> = createEntityAdapter<TodoModel>({
  sortComparer: sortByState,
  selectId: (todo: any) => todo.id
});

export const initialTodosState: TodosState = adapter.getInitialState({
  allTodosSuccess: false,
  selectedId: '',
});

// get the selectors
export const {
  selectEntities,
  selectAll,
  selectIds,
  selectTotal
} = adapter.getSelectors();

export const getSelectedTodoId = (state: TodosState) => state.selectedId;

export const reducer = createReducer<TodosState>(
  initialTodosState,
  on(todoActions.loadTodosSuccess, (state, { todos }) => adapter.setAll(todos, state)),
  on(todoActions.updateTodo, (state, { todo }) => adapter.updateOne(todo, state)),
  on(todoActions.loadId, (state, { id }) => ({ ...state, selectedId: id })),
  on(todoActions.addTodoSuccess, (state, { todo }) => adapter.addOne(todo, state))
);

export function todosReducer(state: TodosState | undefined, action: Action) {
  return reducer(state, action);
}
