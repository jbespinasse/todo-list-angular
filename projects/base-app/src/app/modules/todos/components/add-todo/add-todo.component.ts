import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { addTodo } from '../../store/actions/todos.actions';
import { TodosState } from '../../store/reducers/todo.reducer';
import { TodosActionsType } from '../../store/actions/todos.actions'

@Component({
  selector: 'app-add-todo',
  templateUrl: './add-todo.component.html',
  styleUrls: ['./add-todo.component.scss']
})
export class AddTodoComponent {

  public createForm: FormGroup;
  
  constructor(private store: Store<TodosState>,
    private formBuilder: FormBuilder,
    private router: Router,
    addTodoAction$: Actions) {
      
    this.createForm = this.formBuilder.group({
      title: ['', Validators.required],
      description: [''],
      state: [true]
    });
    
    addTodoAction$.pipe(ofType(TodosActionsType.addTodoSuccess))
      .subscribe(() => this.router.navigate(['todos']));
  }

  onSubmit(): void {
    if (!this.createForm.valid) {
      return;
    }

    const newTodo = Object.assign({}, <any>this.createForm.value);
    if (newTodo.state === true) {
      newTodo.state = 'active';
    } else {
      newTodo.state = 'done';
    }

    this.store.dispatch(addTodo({ todo: newTodo }));
  }
}
