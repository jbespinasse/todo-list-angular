import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { TodoModel } from '../../models/todo-model';
import { TodosState } from '../../store/reducers/todo.reducer';
import { getSelectedTodo } from '../../store/selectors/todos.selector';

@Component({
  selector: 'app-show-todo',
  templateUrl: './show-todo.component.html',
  styleUrls: ['./show-todo.component.scss']
})
export class ShowTodoComponent implements OnInit {

  public todo$: Observable<TodoModel | undefined> = this.store.select(getSelectedTodo);

  constructor(private store: Store<TodosState>) {}

  ngOnInit(): void {
  }

}
