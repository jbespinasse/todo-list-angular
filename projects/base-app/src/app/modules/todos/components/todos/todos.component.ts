import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { TodoModel } from '../../models/todo-model';
import { TodosState } from '../../store/reducers/todo.reducer';
import { loadTodos, updateTodo } from '../../store/actions/todos.actions';
import { getAllTodosSortedByIdDesc } from '../../store/selectors/todos.selector';
import { Update } from '@ngrx/entity';
import { Router } from '@angular/router';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.scss'],
  providers: []
})
export class TodosComponent implements OnInit {

  public displayedColumns: string[] = ['title', 'state', 'check', 'actions'];
  public todos$: Observable<TodoModel[]>;

  constructor(private store: Store<TodosState>,
    private router: Router) {
    this.todos$ = this.store.select<any>(getAllTodosSortedByIdDesc);
  }

  ngOnInit(): void {
    this.store.dispatch(loadTodos());
  }

  updateTodoState(t: TodoModel): void {
    const todo: Update<TodoModel> = (t.state === 'active')
      ? { id: t.id, changes: { state: 'done' } }
      : { id: t.id, changes: { state: 'active' } }
    ;
    this.store.dispatch(updateTodo({ todo }));
  }

  editTodo(todo: TodoModel) {
    this.router.navigate([`todos/show/${todo.id}`]);
  }

  redirect2AddTodo(): void {
    this.router.navigate(['todos/add']);
  }
}
