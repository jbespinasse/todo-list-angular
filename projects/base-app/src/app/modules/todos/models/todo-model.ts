export interface TodoModel {
    id: number;
    title: string;
    description?: string;
    state: string;
    changes: any;
}
