import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from '../../../services/http/http.service';
import { TodoModel } from '../models/todo-model';

@Injectable({
  providedIn: 'root'
})
export class TodosRepository {

  constructor(private http: HttpService) { }

  getAll(): Observable<TodoModel[]> {
    return this.http.get('todos');
  }

  updateById(id: number, todos: any): Observable<TodoModel> {
    return this.http.put(`todos/${id}`, todos);
  }

  getById(id: number): Observable<TodoModel> {
    return this.http.get(`todos/${id}`);
  }

  add(todo: any): Observable<TodoModel> {
    return this.http.post(`todos`, todo);
  }
}
