import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PowerTableComponent } from './power-table.component';



@NgModule({
  declarations: [
    PowerTableComponent
  ],
  imports: [
    CommonModule
  ]
})
export class PowerTableModule { }
