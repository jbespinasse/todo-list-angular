const math = require('mathjs')

const evalNumeric = (operation, scope, round) => {
    const result = math.evaluate(operation, scope);
    console.log(result)
}

const evalString = (operation, scope) => {
    const keys = Object.keys(scope);
    const regex = new RegExp(`${keys.join('|')}`, 'g');
    const result = operation.replace(regex, (x) => scope[x] ? scope[x] : x);
    console.log(result)
}

evalString(
    "(column_name_1 + column_name_2) * column_name_3 / (column_name_1 / 2)",
    {
        column_name_1: 34,
        column_name_2: 20,
        column_name_3: 3
    }
)

evalNumeric(
    "(column_name_1 + column_name_2) * column_name_3 / (column_name_1 / 2)",
    {
        column_name_1: 34,
        column_name_2: 20,
        column_name_3: 3
    },
    2
)

// const condition = (condition: {column_name: string, value: any, operator: string}[][], type: 'or' | 'and', scope) => {
/* const condition = (conditions, type, scope) => {
    for (const x in conditions) {
        for (const y in conditions[x]) {
            const keys = Object.keys(conditions[x][y])
            console.log(keys)
        }
    }
}

condition(
    [[{
        column_name: 'column_name_1',
        value: 34,
        operator: '='
    }, {
        column_name: 'column_name_2',
        value: 28,
        operator: '>'
    }], [{
        column_name: 'column_name_2',
        value: 19,
        operator: '<'
    }, {
        column_name: 'column_name_3',
        value: 3,
        operator: '='
    }]],
    'or',
    {
        column_name_1: 34,
        column_name_2: 20,
        column_name_3: 3
    }
) */