import { math } from 'mathjs'

export const evalNumeric = (operation, scope, round) => {
    return math.evaluate(operation, scope);
}