import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';

if (!localStorage.getItem('todos')) {
  localStorage.setItem('todos', JSON.stringify([
    { id: 1, title: 'Title 1', state: 'active'},
    { id: 2, title: 'Title 2', state: 'active'},
    { id: 3, title: 'Title 3', state: 'active'},
    { id: 4, title: 'Title 4', state: 'active'},
    { id: 5, title: 'Title 5', state: 'active'},
    { id: 6, title: 'Title 6', state: 'active'}
  ]));
}
let todos = JSON.parse(<string>localStorage.getItem('todos')) || [];

@Injectable({
  providedIn: 'root'
})
export class HttpClientInterceptor implements HttpInterceptor {

  constructor() { }

  intercept(request: HttpRequest<any>,
    next: HttpHandler): Observable<HttpEvent<any>> {
    const { url, method, headers, body } = request;

    return of(null).pipe(
      mergeMap(handleRoute),
      materialize(),
      delay(500),
      dematerialize()
    );

    function handleRoute(): Observable<HttpEvent<any>> {
      switch (true) {
        case url.endsWith('/todos') && method === 'GET':
          return getTodos();
        case url.match(/\/todos\/\d+$/) && method === 'PUT':
          return updateTodoById(body);
        case url.match(/\/todos\/\d+$/) && method === 'GET':
          return getTodoById();
        case url.match(/\/todos/) && method === 'POST':
          return addTodo(body);
        case url.match(/\/todos\/\d+$/) && method === 'DELETE':
          return deleteTodo();
        default:
          return next.handle(request);
      }
    }

    function getTodos() {
      return ok(todos);
    }

    function updateTodoById(body?: any) {
      try {
        const id = idFromUrl();
        for (let i = 0; i < todos.length; i++) {
          if (todos[i].id === id) {
            const t = Object.assign([], todos);
            t[i] = { ...t[i], ...body };
            todos = t;
            localStorage.setItem('todos', JSON.stringify(t));
            return ok(body);
          }
        }
        return notOk();
      } catch (err) {
        console.error(err);
        return notOk();
      }
    }

    function addTodo(body: any) {
      try {
        const t = Object.assign([], todos);
        let id = Math.max.apply(Math, todos.map((o: any) => o.id));
        id++;
        const newTodo = {...body, ...{ id }};
        t.push(newTodo);
        todos = t;
        localStorage.setItem('todos', JSON.stringify(t));
        return ok(newTodo);
      } catch (err) {
        console.error(err);
        return notOk();
      }
    }

    function getTodoById() {
      const todo = todos.find((x: any) => x.id == idFromUrl());
      return ok(todo);
    }

    function deleteTodo() {
      todos = todos.filter((x: any) => x.id !== idFromUrl());
      localStorage.setItem('todos', JSON.stringify(todos));
      return ok();
    }

    function ok(body?: any) {
      return of(new HttpResponse({ status: 200, body }));
    }

    function notOk(body?: any) {
      return of(new HttpResponse({ status: 500, body }));
    }

    function idFromUrl() {
      const urlParts = url.split('/');
      return parseInt(urlParts[urlParts.length - 1]);
    }
  }
}
