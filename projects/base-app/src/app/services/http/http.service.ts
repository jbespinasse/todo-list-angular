import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) { }

  get<T>(uri: string): Observable<T> {
    return this.http.get<T>(`${environment.urlApi}${uri}`);
  }

  post<T>(uri: string, params: any): Observable<T> {
    return this.http.post<T>(`${environment.urlApi}${uri}`, params);
  }

  put<T>(uri: string, params: any): Observable<T> {
    return this.http.put<T>(`${environment.urlApi}${uri}`, params);
  }
}
