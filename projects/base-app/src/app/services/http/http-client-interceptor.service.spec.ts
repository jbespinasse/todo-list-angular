import { TestBed } from '@angular/core/testing';

import { HttpClientInterceptor } from './http-client-interceptor.service';

describe('HttpBackendInterceptorService', () => {
  let service: HttpClientInterceptor;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HttpClientInterceptor);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
