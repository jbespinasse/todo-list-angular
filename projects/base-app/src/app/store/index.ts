import {
  ActionReducer,
  ActionReducerMap,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import * as fromTodos from '../modules/todos/store/reducers/todo.reducer'

export function debug(reducer: ActionReducer<any>): ActionReducer<any> {
  return (state: any, action: any) => {
    // console.log('state', state);
    // console.log('action', action);
    return reducer(state, action);
  };
}

export interface AppState {
  todos: fromTodos.TodosState;
}

export const reducers: ActionReducerMap<AppState> = {
  todos: fromTodos.todosReducer
};

export const metaReducers: MetaReducer<AppState>[] = !environment.production ? [debug] : [];